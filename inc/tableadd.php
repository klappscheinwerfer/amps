<!-- TODO: Add tags in form -->
<!-- Last page: 95157 -->

<?php
	if (isset($_POST["add"]))
	{
		$url_text = $_POST["url_text"];
		$url_reason = $_POST["url_reason"];

		$result = pg_prepare($conn, "url_add", "INSERT INTO Url (url_text, url_reason, fk_list_id) VALUES ($1, $2, 1) LIMIT 1 RETURNING url_id");
		$result = pg_execute($conn, "url_add", array($url_text, $url_reason));

		while ($row = pg_fetch_row($result)) {
			$url_id = $row[0];
		}

		# TODO: Add tag selection
		$result = pg_prepare($conn, "assigned_tag_add", "INSERT INTO AssignedTag (fk_tag_id, fk_url_id) VALUES (1, $1)");
		$result = pg_execute($conn, "assigned_tag_add", array($url_id));
	}
?>

<form action="" method="post">
	<input type="text" placeholder="Url" name="url_text" id="" required>
	<input type="text" placeholder="Reason" name="url_reason" id="" required>
	<button type="submit" name="add">Add</button>
</form>