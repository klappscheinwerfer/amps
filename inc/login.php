<?php
	session_start();
	if (isset($_SESSION["username"])) {
		header("location:../index.php");
	}

	if (isset($_POST["login"]))
	{
		include "connect.php";

		$username = $_POST["username"];
		$password = $_POST["password"];

		$result = pg_query_params($conn, "SELECT ampsuser_password FROM Ampsuser WHERE ampsuser_name = $1", array($username));
		$pwd_hash = (pg_fetch_row($result)[0]);
		
		if (password_verify($password, $pwd_hash)) {
			$_SESSION["username"] = $username;
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>AMPS | Log in</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="/tpl/css/styles.css">
		<link rel="stylesheet" href="/tpl/css/login.css">
	</head>
	<body>
		<div id="login-form">
			<form action="" method="post">
				<h1>AMPS</h1>
				<input type="text" placeholder="username" name="username" required>
				<input type="password" placeholder="password" name="password" required>
				<button type="submit" name="login">Login</button>
			</form>
		</div>
	</body>
</html>