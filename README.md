# amps

AMPS web interface

### Repositories

* AMPS web interface: https://gitlab.com/klappscheinwerfer/amps
* AMPS config: https://gitlab.com/klappscheinwerfer/amps-config

## Requirements

* php-pgsql

## Notes

* Run development server: `php -S localhost:8000`
* Default admin login (see amps-config):
	* username: admin
	* password: password