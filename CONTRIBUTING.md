## Development environmnet setup (debian)

* Install git `apt install git`
* Make a directory and move to it, for example `mkdir Repos; cd Repos`
* Clone this repository `git clone https://gitlab.com/klappscheinwerfer/amps`
* Create a postgres database (see db-setup.sh in amps-config)
* Populate the database (see ampsdb_put.py in amps-config)
	* Edit the script if you're not using the default parameters
	* This can take from 5 - 20 minutes
* Run development server: `php -S localhost:8000`

## Style

* Language: English
* Indentations: tabs