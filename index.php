<?php require "inc/loginheader.php"; ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>AMPS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="tpl/css/styles.css">
	</head>
	<body>
		<?php include_once("inc/header.php"); ?>
		<div class="content">
		<?php
			require "inc/connect.php";
			
			include_once("inc/tableadd.php");
			include_once("inc/tablenav.php");

			$result = pg_query_params($conn,
				"SELECT Url.url_id, Url.url_text, Url.url_reason, Tag.tag_name, Tag.tag_color
				FROM Url
				INNER JOIN Assignedtag
					ON Assignedtag.fk_url_id = Url.url_id
				INNER JOIN Tag
					ON Assignedtag.fk_tag_id = Tag.tag_id
				ORDER BY url_id ASC, tag_name ASC
				LIMIT " . $limit . " OFFSET ". $pagenum * $limit . ";",
				array());

			echo "
				<table>
					<tr>
						<th></th>
						<th>Url</th>
						<th>Reason</th>
						<th>Tags</th>
					</tr>
			";
			$prev = -1;
			while ($row = pg_fetch_row($result)) {
				$tag = "<span class='url-tag' style='background:#" . $row[4] . "'>" . $row[3] . "</span>";
				if ($row[0] == $prev) {
					echo $tag;
				} else {
					if ($prev > -1) {
						echo "</td></tr>";
					}
					echo "
						<tr>
							<td><input name='checkbox' type='checkbox' value=" . $row[0] . "></td>
							<td>$row[1]</td>
							<td>$row[2]</td>
							<td>$tag
					";
				}
				$prev = $row[0];
			}
			echo "</td></tr></table>";
		?>
		</div>
		<?php include_once("inc/footer.php"); ?>
	</body>
</html>